from django.contrib import admin

from .models import Person, Buku
admin.site.register(Person)
admin.site.register(Buku)
# Register your models here.
